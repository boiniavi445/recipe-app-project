# Validate gitlab with credit card details to run CICD pipeline
# Recipe App API Proxy

NGINX proxy app for out recipe app API

# Users

# Environment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)